const CIRCLE_RADIUS = 10;
const CIRCLE_FILL = 'cadetblue';
const SPIRAL_SIZE = 5;

const initCanvas = () => {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  document.body.append(canvas);
  return {
    canvas,
    context
  };
}

const { canvas, context } = initCanvas();

const drawCircle = ({x, y}) => {
  context.beginPath();
  context.arc(x, y, CIRCLE_RADIUS, 0, 2 * Math.PI, false);
  context.fillStyle = CIRCLE_FILL;
  context.fill();
}

let step = 1;
const centerX = canvas.width / 2;
const centerY = canvas.height / 2;

const animate = (timestamp) => {
  const x = step;
  const y = -1 * x*x;
  drawCircle({x: centerX + x, y: centerY + y});
  drawCircle({x: centerX - x, y: centerY + y});
  step++;

  console.log(timestamp);
  window.requestAnimationFrame(animate)
};

window.requestAnimationFrame(animate)